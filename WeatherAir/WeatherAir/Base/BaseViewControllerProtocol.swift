//
//  BaseViewControllerProtocol.swift
//  WeatherAir
//
//  Created by Oanh tran on 9/12/19.
//  Copyright © 2019 activeCog. All rights reserved.
//

protocol BaseViewControllerProtocol: class {
    
    associatedtype ViewModelType
    
    var viewModel: ViewModelType! { get set }
    
}

