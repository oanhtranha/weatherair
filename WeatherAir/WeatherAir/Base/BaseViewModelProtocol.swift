//
//  BaseViewModelProtocol.swift
//  WeatherAir
//
//  Created by Oanh tran on 9/12/19.
//  Copyright © 2019 activeCog. All rights reserved.
//

import Foundation

protocol BaseViewModelProtocol {
    
    var router: BaseRouterProtocol { get }
    
}

