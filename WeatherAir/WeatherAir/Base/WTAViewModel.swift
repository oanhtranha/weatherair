//
//  WTAViewModel.swift
//  WeatherAir
//
//  Created by Oanh tran on 9/12/19.
//  Copyright © 2019 activeCog. All rights reserved.
//

import UIKit

class BaseViewModel: NSObject {
    
    override convenience init() {
        self.init()
    }
    
    init(managerProvider: ManagerProvider = .sharedInstance) {
        super.init()
    }
}
