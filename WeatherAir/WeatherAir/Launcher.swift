//
//  Launcher.swift
//  WeatherAir
//
//  Created by Oanh tran on 9/12/19.
//  Copyright © 2019 activeCog. All rights reserved.
//

import UIKit


class Launcher {
    func presentHomeScreen(on window: UIWindow) {
        let router = HomeRouter()
        let viewModel = HomeViewModel.init(router: router)
        let navigationVC = StoryboardScene.Main.instantiateNavigationController()
        guard let viewController = navigationVC.topViewController as? HomeViewController else {
            assertionFailure("Home Storyboard configured not properly")
            return
        }
        viewController.viewModel = viewModel
        router.baseViewController = viewController
        window.rootViewController = navigationVC
    }
}
