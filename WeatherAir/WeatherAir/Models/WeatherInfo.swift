//
//  WeatherInfo.swift
//  WeatherAir
//
//  Created by Oanh Tran on 9/21/19.
//  Copyright © 2019 activeCog. All rights reserved.
//

import UIKit

struct WeatherInfo {
    var statusInfos: [StatusInfo]
    
    init(statusInfos: [StatusInfo]) {
        self.statusInfos = statusInfos
    }
}

struct StatusInfo {
    var icon: UIImage?
    var title: String
    var value: Double
    var unitOfValue: String
    
    init(icon: UIImage?, title: String, value: Double, unit: String) {
        self.icon = icon
        self.title =  title
        self.value = value
        self.unitOfValue = unit
    }
}
