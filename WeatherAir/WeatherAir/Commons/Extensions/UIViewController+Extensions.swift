//
//  UIViewController+Extensions.swift
//  WeatherAir
//
//  Created by Oanh tran on 9/12/19.
//  Copyright © 2019 activeCog. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController {
    
    var top: UIViewController? {
        if let controller = self as? UINavigationController {
            return controller.topViewController?.top
        }
        if let controller = self as? UISplitViewController {
            return controller.viewControllers.last?.top
        }
        if let controller = self as? UITabBarController {
            return controller.selectedViewController?.top
        }
        if let controller = presentedViewController {
            return controller.top
        }
        return self
    }
}

extension UIBarButtonItem {
    class var back_empty: UIBarButtonItem {
        return UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }
}

public typealias Identifier = String

public protocol Identifiable: class  {
    static var get_identifier: Identifier { get }
}

public extension Identifiable {
    static var get_identifier: Identifier {
        return String(describing: self)
    }
}
extension UIViewController: Identifiable {}
extension UITableViewCell: Identifiable {}
