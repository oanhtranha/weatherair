//
//  Storyboards.swift
//  WeatherAir
//
//  Created by Oanh tran on 9/12/19.
//  Copyright © 2019 activeCog. All rights reserved.
//

import Foundation
import UIKit

private final class BundleToken {}

protocol StoryboardSegueType: RawRepresentable { }
protocol StoryboardSceneType {
    static var storyboardName: String { get }
}

extension UIViewController {
    func perform<S: StoryboardSegueType>(segue: S, sender: Any? = nil) where S.RawValue == String {
        performSegue(withIdentifier: segue.rawValue, sender: sender)
    }
}

extension StoryboardSceneType {
    static func storyboard() -> UIStoryboard {
        return UIStoryboard(name: self.storyboardName, bundle: Bundle(for: BundleToken.self))
    }
    
    static func initialViewController() -> UIViewController {
        guard let vc = storyboard().instantiateInitialViewController() else {
            fatalError("Failed to instantiate initialViewController for \(self.storyboardName)")
        }
        return vc
    }
}

extension StoryboardSceneType where Self: RawRepresentable, Self.RawValue == String {
    
    func viewController() -> UIViewController {
        return Self.storyboard().instantiateViewController(withIdentifier: self.rawValue)
    }
    static func viewController(identifier: Self) -> UIViewController {
        return identifier.viewController()
    }
}

enum StoryboardScene {
    enum LaunchScreen: StoryboardSceneType {
        static let storyboardName = "LaunchScreen"
    }
    
    enum Main: String, StoryboardSceneType {
        static let storyboardName = "Main"
        
        case navigationControllerScene = "NavigationController"
        static func instantiateNavigationController() -> UINavigationController {
            guard let vc = StoryboardScene.Main.navigationControllerScene.viewController() as? UINavigationController
                else {
                    fatalError("ViewController 'NavigationController' is not of the expected class UINavigationController.")
            }
            return vc
        }
        
        case WeatherInfosViewController = "WeatherInfosViewController"
        static func instantiateWeatherInfosViewController() -> UIViewController {
            guard let vc = StoryboardScene.Main.WeatherInfosViewController.viewController() as? WeatherInfosViewController
                else {
                    fatalError("ViewController 'WeatherInfosViewController' is not of the expected class WeatherInfosViewController.")
            }
            return vc
        }
    }
    
}
