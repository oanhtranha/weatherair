//
//  WeatherInfosViewModel.swift
//  WeatherAir
//
//  Created by Oanh Tran on 9/14/19.
//  Copyright © 2019 activeCog. All rights reserved.
//

import Foundation
import UIKit

protocol WeatherInfosViewModelProtocol: BaseViewModelProtocol {
    var statusInfos: [StatusInfo] { get }
}

class WeatherInfosViewModel: BaseViewModel, WeatherInfosViewModelProtocol {
    
    let router: BaseRouterProtocol
    var statusInfos: [StatusInfo] = []
    
    init(router: BaseRouterProtocol, managerProvider: ManagerProvider = .sharedInstance) {
        self.router = router
        super.init(managerProvider: managerProvider)
        self.statusInfos =  initStatusInfos()
    }
    
    private func initStatusInfos() -> [StatusInfo] {
        var statusInfos: [StatusInfo] = []
        statusInfos.append(StatusInfo(icon: UIImage(named: "uvIndexImage"), title: "UV Index", value: 11, unit: ""))
        statusInfos.append(StatusInfo(icon: UIImage(named: "feelsLikeImage"), title: "Feels Like", value: 36, unit: "ºC"))
        statusInfos.append(StatusInfo(icon: UIImage(named: "rainImage"), title: "Chance of Rain", value: 25, unit: "%"))
        statusInfos.append(StatusInfo(icon: UIImage(named: "windImage"), title: "Wind Speed", value: 25, unit: "%"))
        return statusInfos
    }
}
