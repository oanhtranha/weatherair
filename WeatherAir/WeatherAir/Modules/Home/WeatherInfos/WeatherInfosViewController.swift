//
//    } WeatherInfosViewController.swift
//  WeatherAir
//
//  Created by Oanh Tran on 9/14/19.
//  Copyright © 2019 activeCog. All rights reserved.
//

import UIKit

class WeatherInfosViewController: WTAViewController, BaseViewControllerProtocol {

    var viewModel: WeatherInfosViewModelProtocol!
    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        guard let tableViewHome = tableView else {
            return
        }
        tableViewHome.reloadData()
    }
}

extension WeatherInfosViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.row {
        case 1:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: AddressHomeCell.get_identifier, for: indexPath) as? AddressHomeCell else {
                return UITableViewCell()
            }
            return cell
        case 2:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: OthersInfoHomeCell.get_identifier, for: indexPath) as? OthersInfoHomeCell else {
                return UITableViewCell()
            }
            cell.loadData(statusInfos: viewModel.statusInfos)
            return cell
        case 3:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: AirQualityHomeCell.get_identifier, for: indexPath) as? AirQualityHomeCell else {
                return UITableViewCell()
            }
            return cell
        default:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: TemperatureHomeCell.get_identifier, for: indexPath) as? TemperatureHomeCell else {
                return UITableViewCell()
            }
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.row {
        case 1:
            return 90
        case 2:
            return 150
        case 3:
            return 100
        default:
            return 130
        }
    }
}
