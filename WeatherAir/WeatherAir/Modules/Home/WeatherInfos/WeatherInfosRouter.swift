//
//  WeatherInfosRouter.swift
//  WeatherAir
//
//  Created by Oanh Tran on 9/14/19.
//  Copyright © 2019 activeCog. All rights reserved.
//

import UIKit

class WeatherInfosRouter: BaseRouterProtocol {
    
    weak var baseViewController: UIViewController?
    
    enum RouteType {
        
    }
    
    func dismiss(animated: Bool, context: Any?, completion: ((Bool) -> Void)?) {
        
    }
    
    func present(on baseVC: UIViewController, animated: Bool, context: Any?, completion: ((Bool) -> Void)?) {
        
    }
    
    func enqueueRoute(with context: Any?, animated: Bool, completion: ((Bool) -> Void)?) {
        
    }
    
}

