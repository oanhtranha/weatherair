//
//  HomeViewController.swift
//  WeatherAir
//
//  Created by Oanh tran on 9/12/19.
//  Copyright © 2019 activeCog. All rights reserved.
//

import UIKit
import FloatingPanel

class HomeViewController: WTAViewController, BaseViewControllerProtocol {

    @IBOutlet weak var topContenView: UIView!
    @IBOutlet weak var button: UIButton!
    @IBOutlet weak var label: UILabel!
    var viewModel: HomeViewModelProtocol!
    var fpc: FloatingPanelController!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Initialize a `FloatingPanelController` object.
        fpc = FloatingPanelController()

        // Assign self as the delegate of the controller.
        fpc.delegate = self // Optional

        // Set a content view controller.
        let router = WeatherInfosRouter()
        let viewModel = WeatherInfosViewModel.init(router: router)
        guard let viewController = StoryboardScene.Main.instantiateWeatherInfosViewController() as? WeatherInfosViewController else { return }
        viewController.viewModel = viewModel
        router.baseViewController = viewController
        fpc.set(contentViewController: viewController)

        // Track a scroll view(or the siblings) in the content view controller.
        fpc.track(scrollView: viewController.tableView)

        // Add and show the views managed by the `FloatingPanelController` object to self.view.
        fpc.addPanel(toParent: self)
    }
}

extension HomeViewController: FloatingPanelControllerDelegate {
    func floatingPanel(_ vc: FloatingPanelController, layoutFor newCollection: UITraitCollection) -> FloatingPanelLayout? {
        return MyFloatingPanelLayout(tipValue: self.view.frame.height - self.topContenView.frame.height, halfValue: self.view.frame.width - 50, fullValue: 16)
    }
}

class MyFloatingPanelLayout: FloatingPanelLayout {
    private let tip: CGFloat
    private let half: CGFloat
    private let full: CGFloat

    init(tipValue: CGFloat, halfValue: CGFloat, fullValue: CGFloat) {
        self.tip = tipValue
        self.half = halfValue
        self.full = fullValue
    }
    public var initialPosition: FloatingPanelPosition {
        return .half
    }

    public func insetFor(position: FloatingPanelPosition) -> CGFloat? {
        switch position {
        case .full: return full // A top inset from safe area
        case .half: return half // A bottom inset from the safe area
        case .tip: return tip // A bottom inset from the safe area
        default: return nil // Or `case .hidden: return nil`
        }
    }
}
