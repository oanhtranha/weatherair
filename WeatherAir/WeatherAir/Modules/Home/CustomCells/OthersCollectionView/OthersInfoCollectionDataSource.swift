//
//  OthersInfoCollectionDataSource.swift
//  WeatherAir
//
//  Created by Oanh Tran on 9/21/19.
//  Copyright © 2019 activeCog. All rights reserved.
//

import Foundation
import UIKit

class OthersInfoCollectionDataSource: NSObject, UICollectionViewDataSource {
    
    var statusInfos: [StatusInfo] = []
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        numberOfItemsInSection section: Int) -> Int {
        return statusInfos.count
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView
            .dequeueReusableCell(withReuseIdentifier: "OthersCollectionCell",
                                 for: indexPath) as? OthersCollectionCell else {
                fatalError()
        }
        let statusInfo = statusInfos[indexPath.row]
        cell.setup(with: statusInfo)
        return cell
    }
    
}
