//
//  OthersCollectionCell.swift
//  WeatherAir
//
//  Created by Oanh Tran on 9/21/19.
//  Copyright © 2019 activeCog. All rights reserved.
//

import UIKit

class OthersCollectionCell: UICollectionViewCell {
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var valueLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    func setup(with statusInfo: StatusInfo) {
        imageView.image = statusInfo.icon
        titleLabel.text =  statusInfo.title
        valueLabel.text = "\(statusInfo.value)\(statusInfo.unitOfValue)"
    }
}
