//
//  TemperatureHomeCell.swift
//  WeatherAir
//
//  Created by Oanh Tran on 9/14/19.
//  Copyright © 2019 activeCog. All rights reserved.
//

import UIKit

class TemperatureHomeCell: UITableViewCell {
    @IBOutlet weak var currentTempLabel: UILabel!
    @IBOutlet weak var upTempLabel: UILabel!
    @IBOutlet weak var downTempLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
    }

}
