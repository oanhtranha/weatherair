//
//  AirQualityHomeCell.swift
//  WeatherAir
//
//  Created by Oanh Tran on 9/21/19.
//  Copyright © 2019 activeCog. All rights reserved.
//

import UIKit

class AirQualityHomeCell: UITableViewCell {

    @IBOutlet weak var airQualityTitleLabel: UILabel!
    @IBOutlet weak var airQualityValueLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    
}
