//
//  AddressHomeCell.swift
//  WeatherAir
//
//  Created by Oanh Tran on 9/21/19.
//  Copyright © 2019 activeCog. All rights reserved.
//

import UIKit

class AddressHomeCell: UITableViewCell {
    @IBOutlet weak var datetimeLabel: UILabel!
    @IBOutlet weak var addressTempLabel: UILabel!
    @IBOutlet weak var statusOfTempLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
    }
}
