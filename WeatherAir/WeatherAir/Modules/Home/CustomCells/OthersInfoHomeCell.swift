//
//  OthersInfoHomeCell.swift
//  WeatherAir
//
//  Created by Oanh Tran on 9/21/19.
//  Copyright © 2019 activeCog. All rights reserved.
//

import UIKit

class OthersInfoHomeCell: UITableViewCell {

    @IBOutlet weak var collectionView: UICollectionView!
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func loadData(statusInfos: [StatusInfo]) {
        guard statusInfos.count > 0, let othersInfoCollectionDataSource = collectionView.dataSource as? OthersInfoCollectionDataSource else { return }
        othersInfoCollectionDataSource.statusInfos = statusInfos
        collectionView.reloadData()
    }
}

// MARK: - Collection View Flow Layout Delegate
extension OthersInfoHomeCell : UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 78, height: 140)
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        insetForSectionAt section: Int) -> UIEdgeInsets {
        return .zero
    }
}
