
//
//  HomeViewModel.swift
//  WeatherAir
//
//  Created by Oanh tran on 9/12/19.
//  Copyright © 2019 activeCog. All rights reserved.
//

import Foundation

protocol HomeViewModelProtocol: BaseViewModelProtocol {
    
}

class HomeViewModel: BaseViewModel, HomeViewModelProtocol {
    
    let router: BaseRouterProtocol
    
    init(router: BaseRouterProtocol, managerProvider: ManagerProvider = .sharedInstance) {
        self.router = router
        super.init(managerProvider: managerProvider)
    }
}
